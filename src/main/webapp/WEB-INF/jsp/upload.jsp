<%@ include file="taglib.jsp" %>
<%@ page language="java" errorPage="/error.jsp" contentType="text/html; charset=utf-8" %>
<%@page pageEncoding="UTF-8" %>
<meta charset='utf-8'>
<!DOCTYPE html>
<html>
<body>

<div class="container">

    <jsp:include page="inner/header.jsp" />

    <form:form modelAttribute="uploadItem" method="post" enctype="multipart/form-data" acceptCharset="UTF-8">
        <fieldset>
                <legend class="text-center">Upload Fields</legend>
            <div class="span3"></div>
            <div class="span3">
                <p>
                    <form:label for="name" path="name">Name*</form:label><br/>
                    <form:input path="name" cssClass="emp"/>
                    <span id = "name_warning" class="alert-error"
                        <c:if test="${not nameValidationFailed}">
                            hidden="hidden"
                        </c:if>
                        >name validation failed</span>
                    <span id = "name_warning" class="alert-error"
                            <c:if test="${not nameOccupied}">
                                hidden="hidden"
                            </c:if>
                            >name occupied, select another name</span>
                </p>
                <form:label for="description" path="description">Description*</form:label>
                <p>
                    <form:textarea path="description" rows="10" cols="40"/>
                     <span id = "name_warning" class="alert-error"
                         <c:if test="${not descrValidationFailed}">
                             hidden="hidden"
                         </c:if>
                     >description validation failed</span>
                </p>

                <p>
                    <form:select path="type">
                        <form:option value="0" label="Select type"/>
                        <form:options items="${types}" itemValue="id" itemLabel="name"/>
                    </form:select>
                    <span id = "name_warning" class="alert-error"
                            <c:if test="${not typeNotSelected}">
                                hidden="hidden"
                            </c:if>
                            >select one of types</span>
                </p>
            </div>
            <div class="span5">

                <c:if test="${validateZipMessage}">
                    <p>
                        <span style="color:red">select zip file please</span>
                    </p>
                </c:if>
                <p>
                    <form:label for="fileData" path="fileData">Select Main File: must be ZIP</form:label><br/>
                    <form:input path="fileData" type="file"/>
                    <span class="span3"/>
                    <span id = "name_warning" class="alert-error"
                            <c:if test="${not fileValidationFailed}">
                                hidden="hidden"
                            </c:if>
                            >this file is not zip OR it's not valid</span>

                    <span id = "name_warning" class="alert-error"
                            <c:if test="${not notuniqueZip}">
                                hidden="hidden"
                            </c:if>
                            >this file was already loaded</span>
                </p>

                <p class="span3">
                    <form:label for="validateFile" path="validateFile">Needs to be validated</form:label>
                    <form:checkbox path="validateFile"/>
                </p>

            </div>
            <legend class="text-center">
                <input id="submit_upload" class="btn btn-primary"  type="submit" disabled="disabled"/>
            </legend>

        </fieldset>
    </form:form>

</div>
</body>

<script type="text/javascript">

    $("#name:input").blur(function(){
        addWarning($(this));
    });

    $("#description").blur(function(){
        addWarning($(this));
    });

    $("#type").change(function(){
        if($(this).find("option:selected").attr('value') == 0){
            $(this).addClass('error');
            $(this).parent().find('.alert-error').removeAttr("hidden");
        } else {
            $(this).removeClass('error');
            $(this).parent().find('.alert-error').attr("hidden","hidden");
        }
        checkSubmit();
    });

    function addWarning(element){
        if( !$(element).val() ) {
            $(element).addClass('error');
            $(element).parent().find('.alert-error').removeAttr("hidden");
        } else {
            $(element).removeClass('error');
            $(element).parent().find('.alert-error').attr("hidden","hidden");
        }
        checkSubmit();
    }

    $(document).onload(checkSubmit());

    function checkSubmit(){
        if($("#type").find("option:selected").attr('value') != 0
                && $("#name:input").val()
                && $("#description").val()){
            $("#submit_upload").removeAttr("disabled");
        } else {
            $("#submit_upload").attr("disabled","disabled");
        }
    }

</script>

</html>


