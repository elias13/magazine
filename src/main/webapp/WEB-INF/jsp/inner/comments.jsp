<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>

<body>

<div id="comments" class="container">
    <c:choose>
        <c:when test="${not empty comments}">
            <c:forEach var="comment" items="${comments}">
                <span class="row">
                    <div class="rounded span5">
                        ${comment.user} : ${comment.text}
                    </div>
                </span>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <li class="nav-header text-left">
                no comments for this article yet
            </li>
        </c:otherwise>
    </c:choose>

    <div class="input-group text-left">
        <span class="input-group-addon">Add new comment</span>
        <input type="text" class="form-control" placeholder="Username" onchange="checkUsername(this)">
        <input type="text" class="form-control" placeholder="Comment" onkeypress="return addCommentWithKey(this, event)">
    </div>
</div>
</body>
</html>