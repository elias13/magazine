<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" errorPage="/error.jsp" contentType="text/html; charset=utf-8" %>
<!DOCTYPE html>

    <div id="popular_grid" class="span6">
        <c:forEach var="item" items="${popular}">
            <div class="span1">

                <a href='<c:url value="/item/${item.name}"/>' class="brand"/>
                <c:choose>
                    <c:when test="${not empty item.icon}">
                        <img class="img-circle" src='<c:url value = "/store/${item.name}/${item.icon}"/>'
                             alt="${item.name}" title="${item.name}"/>
                    </c:when>
                    <c:otherwise>
                        <img class="img-rounded" src='<c:url value = "/image/base/no.jpg"/>' alt="${item.name}" title="${item.name}"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </c:forEach>
    </div>
</html>

<script type="text/javascript">


</script>
