<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<div class="row">
    <div class="span5">
        <div class="row nav-header">
           введите необходимые даные чтоб добавить статью
        </div>
        <span class="span7 cut-string">
            <input name = "author" type="text" class="form-control span4" placeholder="Автор(свой username)" onchange="checkUsername(this)">
            <input name = "name" type="text" class="form-control span4" placeholder="Название статьи">
            <input name = "url" type="text" class="form-control span7" placeholder="Ссылка на статью">
            <br>
            <button class="btn btn-small" onclick="addArticle()">Save</button>
        </span>
    </div>
</div>
</html>

