<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>

<body>

<div class="container">
    <div class="row">
        <jsp:include page="popularGrid.jsp"/>
    </div>

    <br>
    <br>

    <div class="row main_content">

        <div class="span2">
            <div class="well">
                <nav>
                    <c:if test="${not empty categories}">
                        <ul id="categoryList" class="nav nav-list">
                            <li class="nav-header" disable="disable">
                                categories
                            </li>
                            <c:forEach var="category" items="${categories}" varStatus="id">
                                <li id="${category.id}">
                                    <a href="#">
                                        ${category.name}
                                    </a>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:if>
                    <li name="addCategory" >
                        <a href="#" onclick="changeVisible($('#addCategory'))">добавить категорию</a>
                    </li>
                </nav>
            </div>
        </div>

        <div class="span10">
            <div id="fileInfoContent" class="well text-center">
                <div id = "addCategory" class="span5 row" style="display: none">
                    <input name = "name" type="text" class="form-control span4" placeholder="Имя категории">
                    <br>
                    <button class="btn btn-small" onclick="addCategory()">Save</button>
                </div>
                <img class="img-rounded" src='<c:url value="/image/base/main.jpg"/>'>
            </div>
        </div>
        <div class="row span2 well">
            <form method="POST" action="<c:url value='/addUser'/>">
                <c:if test="${created == false}">
                    <span class="error">user was not created</span>
                </c:if>
                Create new user
                Username<input type="text" name="username"/>
                Email<input type="text" name="email"/>
                <input type="submit" value="create"/>
            </form>
            <table>
                All current users
                <c:forEach var="user" items="${users}" varStatus="id">
                    <tr><td>${user.name}</td></tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>

</body>
</html>
