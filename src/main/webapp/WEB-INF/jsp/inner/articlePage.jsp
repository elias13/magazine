<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<div id="articlePage" class="container span8 rounded"  href = '${selectedArticle.description}'>
    <div class="row">
        <div id="titleIcon" class="span2">
            <a href = '<c:url value="/item/${selectedArticle.name}"/> '>
                <img class="img-rounded" src='<c:url value = "/image/base/empty.jpg"/>'
                     alt="${selectedArticle.name}" title="${selectedArticle.name}"/>
            </a>
        </div>
        <div class="span5">

            <div class="row nav-header">
                ${selectedArticle.name}
            </div>
            <span class="span4 cut-string">
                <a href = '${selectedArticle.description}'>ссылка на статью </a>
            </span>
            <span class="span4">
                 <c:choose>
                     <c:when test="${not empty selectedArticle.rating}">
                         рейтинг : <span id = "times">${selectedArticle.rating}</span>
                     </c:when>
                     <c:otherwise>
                         рейтинг : <span id = "times">нет оценок</span>
                     </c:otherwise>
                 </c:choose>
            </span>
            <span class="span4">
                <c:choose>
                    <c:when test="${not empty selectedArticle.user}">
                        добавил : <span id = "author">${selectedArticle.user.name}</span>
                    </c:when>
                    <c:otherwise>
                        добавил : <span id = "author">аноним</span>
                    </c:otherwise>
                </c:choose>

            </span>
            <br><br><br>
            <button class="btn btn-primary btn-block" onclick="showOrHideComments(${selectedArticle.id}, this)">show comments</button>
        </div>
    </div>
    <hidden id = articleId value = ${selectedArticle.id}/>
    <div articleId = "${selectedArticle.id}" name="comments" class = "span8" style="display: none">123123</div>
</div>
</html>

