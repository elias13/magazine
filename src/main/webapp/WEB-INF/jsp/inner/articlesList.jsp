<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" errorPage="/error.jsp" contentType="text/html; charset=utf-8" %>
<!DOCTYPE html>

<div id="fileInfoContainer" class="container">
    <c:choose>
        <c:when test="${not empty articles}">
            <c:forEach var="item" items="${articles}">
                <div class="row">
                    <div class="span6">
                        <div class="span1">
                            <a href='<c:url value="/article/${item.id}"/>' class="brand"/>
                            <img class="img-rounded" src='<c:url value = "/image/base/empty.jpg"/>' alt="${item.name}" title="${item.name}"/>
                        </div>
                        ${item.name}
                    </div>
                </div>
                <div class="container" style="display: none">
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <li class="nav-header text-left">
                no files for this article yet
            </li>
        </c:otherwise>
    </c:choose>

    <div class="span6">
        <button class="btn btn-primary btn-block" onclick="displayAddArticleMenu()">добавить статью</button>
        <div id="addArticle" class="container span8 rounded" style="display: none">
            <jsp:include page="addArticle.jsp"/>
        </div>
    </div>
</div>
</html>

<script type = "text/javascript">


</script>
