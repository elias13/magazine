
var prePath;

/*upload the file after click the button download*/
function upload(element, prePath, name, path){

    var currentDownloadCount = $(element).closest(".span5").find("#times");
    $.ajax({
        type:"GET",
        url: prePath + 'check/' + name + '/' + path,
        success:function(htmlRequest){
            if(htmlRequest){
                window.location.href = prePath + 'download/' + name + '/' + path;
                currentDownloadCount.text(1 + parseInt(currentDownloadCount.text()));
                $("#popular_grid").parent().fadeOut("fast");
                setTimeout(function(prePath){reloadHeader(prePath)}, 1000);
            }
        }
    });
}

/* reload popular images*/
function reloadHeader(){

    var heresome = "";
    $.ajax({
        type:"GET",
        url: prePath + "popularGrid",
        success:function(resultHtml){
            /*$("#popular_grid").parent().html(resultHtml);*/
            var parent = $("#popular_grid").parent();
            parent.html(resultHtml);
            parent.fadeIn("fast");
        }
    });
}

 /* when select category in main menu - the appropriate data will download*/
function executeCategoryListAction(prePath){
    $("#categoryList > li").not(".nav-header").click(function(){
        if($(this).attr("name") == "addCategory") {
            return;
        }
        $("#categoryList .active").removeClass("active");
        $(this).addClass("active");

        var id = $(this).attr("id");
        $.ajax({type: "GET",
            url: prePath + 'loadCategory/' + id,
            success: function (responseHtml) {
                $("#fileInfoContent").html(responseHtml.trim());
                executeFileContainerAction();
            },
            error: function (){

            }
        });

    }) ;
}

/* show the detailed file data on click the row in main-view*/
function executeFileContainerAction(prePath){
    $("#fileInfoContainer div.row").click(function() {
        var container = $(this).next();
        if(container.is(':visible')){
            container.slideUp('slow')
        } else {
            $.ajax({
                type:"GET",
                url: $(this).find("a").attr("href"),
                success: function(resultHtml){
                    container.html(resultHtml);
                    container.slideDown("slow")
                }
            });
        }
    });

}

function showOrHideComments(articleId, button) {

    var HIDE_COMMENTS = "hide comments";
    var value = $(button).html();
    var container = $(button).closest("#articlePage").find("div[name=comments]")

    if (value == HIDE_COMMENTS) {
        $(button).html("show comments");
        container.slideUp("slow");
    } else {
        $(button).html("hide comments");
        $.ajax({
            type:"GET",
            url:prePath + "loadComments/" + articleId,
            success:function(resultHtml) {
                container.html(resultHtml);
                container.slideDown("slow");
            }
        });
    }
}

function reloadComments(articleId, element) {
    var container = $(element).closest("div[name=comments]")

        $(button).html("show comments");
        container.slideUp("slow");
        $(button).html("hide comments");
        $.ajax({
            type:"GET",
            url:prePath + "loadComments/" + articleId,
            success:function(resultHtml) {
                container.html(resultHtml);
                container.slideDown("slow");
            }
        });
}

function checkUsername(element){
    $.ajax({
        type:"GET",
        url:prePath + "isUser/" + element.value,
        success:function(response) {
            if (response != "contains") {
                $(element).addClass("error");
            } else {
                $(element).removeClass("error");
            }
        }
    });

}

function addCommentWithKey(element, key) {
    if(key.keyCode  == 13) {
        return addComment(element);
    }
}

function addComment(element){
    //var posting = $.post(prePath + "addComment", {articleId: articleId, userName : username, text: element.value })

    var usernameInput = $(element).prev("input");
    var articleId = $(element).closest("[articleId]").attr('articleId');
    if ($(usernameInput).hasClass("error")) {
        return;
    } else {
        var username = usernameInput.val();
        $.ajax({
            type:"POST",
            url:prePath + "addComment",
            data:{articleId: articleId, userName : username, text: element.value },
            success:function(response) {
                if (response == "error") {
                    $(element).addClass("error");
                } else {
                    var parent = $(element).closest("div[name=comments]");
                    var addon = '<span class="row"> <div class="rounded span5">' + username + ':' + element.value + '</div> </span>';
                    var temp = parent.find(".input-group");
                    parent.find(".input-group").remove();
                    parent.append(addon);
                    parent.append(temp);
                }
            }
        });
    }
}

function displayAddArticleMenu() {
    var container = $("#addArticle");
    if(container.is(':visible')){
        container.slideUp('slow')
    } else {
        container.slideDown('slow')
    }
}

function addArticle() {
    var categoryId = $("#categoryList li.active").attr("id");
    var container = $("#addArticle");

    var usernameInput = container.find("input[name=author]");
    var articleNameInput = container.find("input[name=name]");
    var urlInput = container.find("input[name=url]");

    if(categoryId && container.is(':visible') &&
        (!$(usernameInput).hasClass("error") & notEmpty(articleNameInput) & notEmpty(urlInput))) {


        $.ajax({
            type:"POST",
            url: prePath + 'addArticle/' + categoryId,
            data:{username: usernameInput.val(), articleName : articleNameInput.val(), description: urlInput.val() },
            success: function(response){
                if (response == "error") {
                    $(element).addClass("error");
                } else {
                    $.ajax({type: "GET",
                        url: prePath + 'loadCategory/' + categoryId,
                        success: function (responseHtml) {
                            $("#fileInfoContent").html(responseHtml.trim());
                            executeFileContainerAction();
                        },
                        error: function (){

                        }
                    });
                }
            }
        });
    }
}


function notEmpty(element){
    element = $(element);
    var errorClass = "error";
    var onchangeAttr = "onchange";
    if(!element.val()){
        element.addClass(errorClass);
        if(!element.attr(onchangeAttr)) {
            element.attr(onchangeAttr, "notEmpty(this)");
        }
        return false;
    } else {
        element.removeClass(errorClass);
        element.removeAttr(onchangeAttr);
        return true;
    }

}

function changeVisible(menu) {
    if(menu.is(':visible')){
        menu.slideUp('slow')
    } else {
        menu.slideDown('slow')
    }
}

function addCategory() {
    var container  = $("#addCategory");
    var categoryInput = container.find("input[name=name]");

    if(notEmpty(categoryInput)) {

        $.ajax({
            type:"POST",
            url: prePath + 'addCategory/',
            data:{categoryName: categoryInput.val()},
            success: function(response){
                if (!response) {
                    $(categoryInput).addClass("error");
                } else {
                    var id = response.id;
                    var name = response.name;
                    $("#categoryList").append("<li id = " + id + "><a href='#'>" + name + "</a></li>");
                    executeCategoryListAction(prePath);
                }
            }
        });
    }
}




