package com.romashchenko.magazine.repository;

import com.romashchenko.magazine.model.Article;
import com.romashchenko.magazine.model.User;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.Resource;
import java.util.List;

@Resource
public interface UserRepository extends CrudRepository<User, Long> {

    User findByName(String name);
}
