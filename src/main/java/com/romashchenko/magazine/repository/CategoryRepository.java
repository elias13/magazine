package com.romashchenko.magazine.repository;

import com.romashchenko.magazine.model.Category;
import com.romashchenko.magazine.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Ilya
 */
@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    Category findByName(String name);
}
