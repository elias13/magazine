package com.romashchenko.magazine.repository;

import com.romashchenko.magazine.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends CrudRepository<Article, Long> {

    //@Query("select a from ARTICLE a where a.CATEGORY_ID = ?1")
    List<Article> findByCategoryId(final Long categoryId);
}
