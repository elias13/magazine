package com.romashchenko.magazine.repository;

import com.romashchenko.magazine.model.Comments;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.Resource;
import java.util.List;

@Resource
public interface CommentsRepository  extends CrudRepository<Comments, Long> {

    List<Comments> findByArticleId(final Long articleId);
}
