package com.romashchenko.magazine.web;

import com.romashchenko.magazine.model.Article;
import com.romashchenko.magazine.model.Category;
import com.romashchenko.magazine.repository.ArticleRepository;
import com.romashchenko.magazine.repository.CategoryRepository;
import com.romashchenko.magazine.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Simple index page controller serving hello.jsp file 
 */
@Controller
public class HomeController {

    /**
     * Simply serves hello.jsp
     * @return view with name 'hello'
     */

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String handleRequest(Model model,
                                @RequestParam(value = "created", required = false, defaultValue = "true") final boolean created) {

        model.addAttribute("users", userRepository.findAll());
        model.addAttribute("created", created);

        Iterable<Category> categoryList = categoryRepository.findAll();
        model.addAttribute("categories", categoryList);

        return "hello";
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public String handleCustomException(Exception ex) {
        return ex.toString() + "\n" + ex.getLocalizedMessage();
    }

}
