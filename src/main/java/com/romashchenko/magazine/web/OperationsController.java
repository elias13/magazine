package com.romashchenko.magazine.web;

import com.romashchenko.magazine.model.Article;
import com.romashchenko.magazine.model.Category;
import com.romashchenko.magazine.model.Comments;
import com.romashchenko.magazine.model.User;
import com.romashchenko.magazine.repository.ArticleRepository;
import com.romashchenko.magazine.repository.CommentsRepository;
import com.romashchenko.magazine.repository.UserRepository;
import com.romashchenko.magazine.service.ArticleService;
import com.romashchenko.magazine.service.CategoryService;
import com.romashchenko.magazine.service.CommentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class OperationsController {
    //views
    final static String ARTICLES_LIST_VIEW = "inner/articlesList";
    static final String ASYNCH_ARTICLE_PAGE_NAME = "inner/articlePage";

    //model names
    final static String ARTICLES = "articles";
    static final String ARTICLE_MODEL_NAME = "selectedArticle";


    @Resource
    ArticleService articleService;

    @Resource
    CategoryService categoryService;

    @Resource
    ArticleRepository articleRepository;

    @Resource
    CommentsRepository commentsRepository;

    @Resource
    UserRepository userRepository;

    @Resource
    CommentService commentService;

    @RequestMapping(value = "/loadCategory/{categoryId}", method = RequestMethod.GET)
    public ModelAndView loadAllArticlesForCategory(@PathVariable Long categoryId) {
        List<Article> articles = articleService.getAllArticlesByCategory(categoryId);
        return new ModelAndView(ARTICLES_LIST_VIEW, ARTICLES, articles);
    }

    @RequestMapping(value = "/article/{articleId}", method = RequestMethod.GET)
    public ModelAndView getArticle(@PathVariable Long articleId) {
        Article article = articleRepository.findOne(articleId);
        return new ModelAndView(ASYNCH_ARTICLE_PAGE_NAME, ARTICLE_MODEL_NAME, article);
    }

    @RequestMapping(value = "/loadComments/{articleId}", method = RequestMethod.GET)
    public ModelAndView getComments(@PathVariable Long articleId) {
        List<Comments> comments = commentsRepository.findByArticleId(articleId);
        return new ModelAndView("inner/comments", "comments", comments);
    }

    @RequestMapping(value = "/isUser/{username}", method = RequestMethod.GET)
    @ResponseBody
    public String isUser(@PathVariable String username) {
        User user = userRepository.findByName(username);
        return user == null ? "no" : "contains";
    }

    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    @ResponseBody
    public String createComment(@RequestParam(value = "articleId") Long articleId,
                                @RequestParam(value = "userName") String username,
                                @RequestParam(value = "text") String text) {
        return commentService.createComment(articleId, username, text) ? "confirm" : "error";
    }

    @RequestMapping(value = "/addArticle/{categoryId}", method = RequestMethod.POST)
    @ResponseBody
    public String createArticle(@PathVariable Long categoryId,
                                @RequestParam(value = "articleName") String articleName,
                                @RequestParam(value = "username") String username,
                                @RequestParam(value = "description") String description) {
        return articleService.createArticle(categoryId, username, articleName, description) ? "success" : "error";
    }

    @RequestMapping(value = "/addCategory", method = RequestMethod.POST)
    @ResponseBody
    public Category createCategory(@RequestParam(value = "categoryName") String categoryName) {
        return categoryService.createCategory(categoryName);
    }


    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String createUser(@RequestParam(value = "username") final String username,
                             @RequestParam(value = "email") final String email) {
        User newUser = new User();
        newUser.setName(username);
        newUser.setEmail(email);

        if(userRepository.findByName(username) == null) {
            userRepository.save(newUser);
            return "redirect:/?created=true";
        }
        return "redirect:/?created=false";
    }


}
