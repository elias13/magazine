package com.romashchenko.magazine.service;

/**
 * Created by Ilya
 */
public interface CommentService {

    public boolean createComment(Long articleId, String username, String text);
}
