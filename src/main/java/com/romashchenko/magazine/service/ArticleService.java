package com.romashchenko.magazine.service;

import com.romashchenko.magazine.model.Article;
import org.springframework.stereotype.Component;

import java.util.List;

public interface ArticleService {

    List<Article> getAllArticlesByCategory(final Long categoryId);

    boolean createArticle(Long categoryId, String username, String articleName, String description);

}
