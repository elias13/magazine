package com.romashchenko.magazine.service.impl;

import com.romashchenko.magazine.model.Article;
import com.romashchenko.magazine.model.Category;
import com.romashchenko.magazine.model.User;
import com.romashchenko.magazine.repository.ArticleRepository;
import com.romashchenko.magazine.repository.CategoryRepository;
import com.romashchenko.magazine.repository.UserRepository;
import com.romashchenko.magazine.service.ArticleService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ArticleServiceSpringDataImpl implements ArticleService {

    @Resource
    private ArticleRepository articleRepository;
    
    @Resource
    private CategoryRepository categoryRepository;
    
    @Resource
    private UserRepository userRepository;

    @Override
    public List<Article> getAllArticlesByCategory(Long categoryId) {
        return articleRepository.findByCategoryId(categoryId);
    }

    @Override
    public boolean createArticle(Long categoryId, String username, String articleName, String description) {
        Category category = categoryRepository.findOne(categoryId);
        User user = userRepository.findByName(username);
        if(category != null && user != null &&
                !StringUtils.isEmpty(articleName) && !StringUtils.isEmpty(description)) {
            Article addedArticle = new Article();
            addedArticle.setUser(user);
            addedArticle.setCategory(category);
            addedArticle.setDescription(description);
            addedArticle.setName(articleName);

            return articleRepository.save(addedArticle) != null;
        }
        return false;
    }


}
