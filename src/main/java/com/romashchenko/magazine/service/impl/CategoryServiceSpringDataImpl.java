package com.romashchenko.magazine.service.impl;

import com.romashchenko.magazine.model.Article;
import com.romashchenko.magazine.model.Category;
import com.romashchenko.magazine.repository.ArticleRepository;
import com.romashchenko.magazine.repository.CategoryRepository;
import com.romashchenko.magazine.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryServiceSpringDataImpl implements CategoryService {

    @Resource
    private CategoryRepository categoryRepository;

    @Override
    public Category createCategory(String categoryName) {
        if(!StringUtils.isEmpty(categoryName) && categoryRepository.findByName(categoryName) == null) {
            return categoryRepository.save(new Category(categoryName));
        }
        return null;
    }
}
