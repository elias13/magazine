package com.romashchenko.magazine.service.impl;

import com.romashchenko.magazine.model.Article;
import com.romashchenko.magazine.model.Comments;
import com.romashchenko.magazine.model.User;
import com.romashchenko.magazine.repository.ArticleRepository;
import com.romashchenko.magazine.repository.CommentsRepository;
import com.romashchenko.magazine.repository.UserRepository;
import com.romashchenko.magazine.service.CommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class CommentServiceImpl implements CommentService {

    @Resource
    CommentsRepository commentsRepository;

    @Resource
    UserRepository userRepository;

    @Resource
    ArticleRepository articleRepository;

    @Override
    public boolean createComment(Long articleId, String username, String text) {
        
        final User user = userRepository.findByName(username);
        final Article article = articleRepository.findOne(articleId);
        
        if (user != null && article != null) {
            Comments comment = new Comments();
            comment.setArticle(article);
            comment.setUser(user);
            comment.setText(text);
            comment.setDate(new Date());
            commentsRepository.save(comment);
            return true;
        }
        return false;
    }
}
