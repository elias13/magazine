package com.romashchenko.magazine.service;

import com.romashchenko.magazine.model.Article;
import com.romashchenko.magazine.model.Category;
import org.springframework.stereotype.Service;

import java.util.List;


public interface CategoryService {

    Category createCategory(String categoryName);

}
